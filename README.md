# Linux configuration files

A repository that contains all the config files I need for different tools on Linux. I took the idea from [him](https://github.com/alexhernandezgarcia/linux-config-utils).
